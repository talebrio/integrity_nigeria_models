<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AboutController;
use App\Http\Controllers\AllCandidateController;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\CandidateController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\VoteController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/',  [HomeController::class,'home'])->name('home');
Route::get('/about', [AboutController::class,'index'])->name('about');
Route::get('/contact', [ContactController::class,'contact'])->name('contact');
Route::post('/contacts', [ContactController::class,'store'])->name('contact.store');

Route::get('/candidates', [CandidateController::class,'index'])->name('candidates');
Route::get('/candidate/{id}', [CandidateController::class,'show'])->name('candidate.show');

/*** Facebook ***/
Route::get('/auth/facebook', [AuthController::class, 'redirectToFacebook'])->name('auth.facebook');
Route::get('/auth/facebook/callback', [AuthController::class, 'handleFacebookCallback'])->name('auth.facebook.callback');

/*** Paiements ***/
Route::post('/vote/candidate/{number}', [VoteController::class, 'redirectToFlutterWave'])->name('vote');
Route::get('/payment/callback', [VoteController::class, 'flutterWaveCallback'])->name('callback');
Route::post('/payment/webhook', [VoteController::class, 'webhook'])->name('webhook');
Route::get('/payment/success', [VoteController::class, 'successPage']);
Route::get('/payment/cancel', [VoteController::class, 'cancelPage']);
// Route::post('/paiement/notify', [VoteController::class,'paymooneyNotification']);
Route::get('/contact/preview/{id}', [ContactController::class, 'contactMailPreview']);

Route::middleware(['auth','verified'])->group(function () {

    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard.index');
    Route::get('/dashboard/candidates', [AllCandidateController::class, 'show'])->name('dashboard.show');
    Route::get('/dashboard/candidates/create', [AllCandidateController::class, 'create'])->name('dashboard.create');
    Route::post('/dashboard/candidates', [AllCandidateController::class, 'store'])->name('dashboard.store');
    Route::get('/dashboard/candidate/{number}/delete', [AllCandidateController::class, 'destroy'])->name('candidate.destroy');
    // Route::delete('/dashboard/{id}', [AllCandidateController::class, 'destroy'])->name('dashboard.destroy');
    Route::get('/dashboard/candidates/{id}/edit', [AllCandidateController::class, 'edit'])->name('dashboard.edit');
    Route::put('/dashboard/candidate/{id}', [AllCandidateController::class, 'update'])->name('dashboard.update');

}
);

Route::post('/count-down', [HomeController::class, 'update'])->name('countdown.update');


Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';

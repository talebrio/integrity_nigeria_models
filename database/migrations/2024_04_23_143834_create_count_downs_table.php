<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('count_downs', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('days')->default(0);
            $table->unsignedInteger('hours')->default(0);
            $table->unsignedInteger('minutes')->default(0);
            $table->unsignedInteger('seconds')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('count_downs');
    }
};

<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Candidate>
 */
class CandidateFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'firstName' => fake()->firstNameFemale(),
            'lastName' => fake()->lastName(),
            'photoUrl' => '/assets/images/model1.jpg',
            'number' => fake()->numberBetween(1, 50),
            'age' => fake()->numberBetween(21, 25),
            'status' => 'active',
            'occupation' => 'Student',
            'description' => fake()->realText($maxNbChars = 200, $indexSize = 2),
        ];
    }
}

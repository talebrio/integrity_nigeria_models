<?php

namespace App\Http\Controllers;
use App\Models\Candidate;
use App\Models\Vote;
use App\Models\Paiement;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(){
        $candidates = Candidate::all();
        $totalVotes = 0;
        $totalCandidates =  Candidate::all()->count();
        $totalPaiements = Paiement::where('status', 'success')->get();
        $totalAmount = 0;
        $vote= [];
        $allVotes = Vote::all();


            foreach ($candidates as $candidate) {

                $votes = Vote::where('candidate_id', $candidate->id)->get();
                $voteqty =$votes->sum('quantity');
                $vote[$candidate->id] = $voteqty;
            }

            foreach ($totalPaiements as $paiement) {
                $totalAmount = $totalAmount + $paiement->amount;
            }

            foreach ($allVotes as $v) {
                // dd($vote->quantity);
                $totalVotes = $totalVotes + $v->quantity;
            }

        return view('dashboard.dashboard',[
            'candidates'=>$candidates,
            'totalCandidates' => $totalCandidates,
            'totalVotes' => $totalVotes,
            'totalAmount'=> $totalAmount,
            'votes' => $vote
        ]);
    }
}

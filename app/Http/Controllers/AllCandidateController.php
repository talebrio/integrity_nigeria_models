<?php

namespace App\Http\Controllers;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;

use Illuminate\Http\Request;
use App\Models\Candidate;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;


class AllCandidateController extends Controller
{
    public function show(){

        $candidates = Candidate::all();

        return view('dashboard.allCandidates',['candidates'=>$candidates]);
    }

    public function create(){

        return view('dashboard.addCandidate');
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'photoUrl' => 'required|image|max:3000',
            'fname' => 'required',
            'lname' => 'required',
            'number' => 'required|integer|unique:candidates',
            'age' => 'required',
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $path = null;
        if ($request->hasFile('photoUrl')) {
            if (Storage::exists('public/images/candidates') == false) {
                Storage::makeDirectory('public/images/candidates');
            }
            $file = $request->file('photoUrl');
            $itemRef = Str::uuid()->toString();
            $fileName = $itemRef.$file->getClientOriginalName();
            $file->move(public_path('/images/candidates'), $fileName);

            $path = '/images/candidates' . '/' . $fileName;
        }

        $candidate = new Candidate();
        $candidate->photoUrl = $path;
        $candidate->firstName = $request->fname;
        $candidate->lastName = $request->lname;
        $candidate->number = $request->number;
        $candidate->age = $request->age;
        $candidate->status = $request->status;
        $candidate->save();

        session()->flash('success', 'Candidate Created successfully');

        return back();

    }
    public function edit($id){
        $candidate = Candidate::findorFail($id);


        return view('dashboard.editCandidate',['candidate'=>$candidate]);
    }

    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'photoUrl' => 'nullable|image|max:3000',
            'fname' => 'required',
            'lname' => 'required',
            'candidateNo' => 'required',
            'age' => 'required',
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $candidate = Candidate::findOrFail($id);

        $path= null;
        if ( $request->file('photoUrl') != null && $request->hasFile('photoUrl')) {
            if (Storage::exists('public/images/candidates') == false) {
                Storage::makeDirectory('public/images/candidates');
            }
            $file = $request->file('photoUrl');
            $itemRef = Str::uuid()->toString();
            $fileName = $itemRef.$file->getClientOriginalName();
            $file->move(public_path('/images/candidates'), $fileName);

            $path = '/images/candidates' . '/' . $fileName;

            $candidate->photoUrl = $path;

        }


        $candidate->firstName = $request->fname;
        $candidate->lastName = $request->lname;
        $candidate->number = $request->candidateNo;
        $candidate->age = $request->age;
        $candidate->status = $request->status;
        $candidate->save();

        session()->flash('success', 'Candidate updated successfully');

        return back();
        // return redirect('/dashboard/candidates');




    }

    public function destroy($number){
        $candidate = Candidate::firstWhere('number', $number);
        // $imagePath = str_replace('/storage', 'public', $candidate->photoUrl);
        // Storage::delete(Storage::path($imagePath));
        $candidate->delete();
        return back();

    }
}

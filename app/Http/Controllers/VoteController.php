<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;

use App\Models\Candidate;
use App\Models\Paiement;
use App\Models\Vote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use KingFlamez\Rave\Facades\Rave as Flutterwave;

class VoteController extends Controller
{
    public function redirectToFlutterWave(int $number, Request $request)
    {
        $reference = Flutterwave::generateReference();
        $amount = $request->product_quantity * 50;
        $candidate = Candidate::where('number', $number)->first();
        // $itemRef = Str::uuid()->toString();


        $data = array(
            'payment_options' => 'card,banktransfer',
            'amount' => $amount,
            'tx_ref' => $reference,
            'currency' => "NGN",
            'redirect_url' => route('callback'),
            'customer' => [
                'email' => "integrity@integritynigeria.com",
                "name" => $candidate->fullName(),
            ],

            "customizations" => [
                "title" => 'Miss Integrity Nigeria',
                "description" => "$request->qtyInput == 1? '1 vote' : $request->qtyInput.' votes',",

            ]

        );

        $payment = Flutterwave::initializePayment($data);


        if ($payment['status'] !== 'success') {
            // notify something went wrong
            return redirect()->back();
        }
        Paiement::create([
            'item_ref' => $reference,
            'candidate_id' => $candidate->id,
        ]);

        return redirect($payment['data']['link']);
    }

    public function flutterWaveCallback(Request $request)
    {
        

        if ($request->status == 'successful') {
            $transactionID = Flutterwave::getTransactionIDFromCallback();
            $data = Flutterwave::verifyTransaction($transactionID);

            $paiement = Paiement::where('item_ref', $data['data']['tx_ref'])->first();
            if ($paiement->status != 'success') {
                $paiement->amount = $data['data']['amount'];
                $paiement->currency = $data['data']['amount'];
                $paiement->status = $data['status'];
                $paiement->save();

                $vote = Vote::create([
                    'candidate_id' => $paiement->candidate_id,
                    'quantity' => $paiement->amount / 50,
                ]);

                $paiement->vote_id = $vote->id;
                $paiement->save();
            }
         
            return view('pages.paiement.success');
        } else {
            
            return view('pages.paiement.cancel');
        }
    }

    /**
     * Receives Flutterwave webhook
     * @return void
     */
    public function webhook(Request $request)
    {
        Log::info($request->all());
    
        // This verifies the webhook is sent from Flutterwave
        $verified = Flutterwave::verifyWebhook();
    
        // If it is a charge event, verify and confirm it is a successful transaction
        if ($verified && $request->input('event.type') === 'CARD_TRANSACTION' && $request->input('status') === 'successful') {
            $verificationData = Flutterwave::verifyPayment($request->input('id'));
         
                Log::info("All data is good");
                $paiement = Paiement::where('item_ref', $request->input('txRef'))->first();
    
                if ($paiement->status != 'success') {
                    $paiement->amount = $request->input('amount');
                    $paiement->currency = $request->input('currency');
                    $paiement->status = $request->input('status');
                    $paiement->save();
    
                    $vote = Vote::create([
                        'candidate_id' => $paiement->candidate_id,
                        'quantity' => $paiement->amount / 100,
                    ]);
    
                    $paiement->vote_id = $vote->id;
                    $paiement->save();
                }
           
        }
    }

    public function successPage()
    {

        return view('pages.paiement.success');
    }

    public function cancelPage()
    {

        return view('pages.paiement.cancel');
    }
}

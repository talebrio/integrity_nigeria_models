<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Candidate;
use App\Models\Vote;

class CandidateController extends Controller
{
    public function index()
    {

        $candidates = Candidate::all();
        $vote= [];

        foreach ($candidates as $candidate) {

            $votes = Vote::where('candidate_id', $candidate->id)->get();
            $voteqty =$votes->sum('quantity');
            
            $vote[$candidate->id] = $voteqty;
        }

        return view('pages.candidates', [
            'candidates' => $candidates,
            'vote' => $vote,
        ]);
    }

    public function show($id)
    {

        $candidate = Candidate::where('number', $id)->firstOrFail();
        $nbVote = 0;
        $votes = Vote::where("candidate_id",$candidate->id)->get();
        //  $voteqty =$votes->sum('quantity');

        foreach ($votes as $vote) {
            $nbVote = $nbVote + $vote->quantity;
        }

        // dd($votes);
        return view('pages.candidateProfile', ['candidate' => $candidate, 'nbVote'=>$nbVote]);
    }
}

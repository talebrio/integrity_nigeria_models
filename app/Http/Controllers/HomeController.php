<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Candidate;
use App\Models\CountDown;
use App\Models\Vote;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{
    public function home(){

        $candidates = Candidate::all();
        $vote= [];
        
            foreach ($candidates as $candidate) {

                $votes = Vote::where('candidate_id', $candidate->id)->get();
                $voteqty =$votes->sum('quantity');
                $vote[$candidate->id] = $voteqty; 
            }

           // Check if countdown data exists in the database
        $countdown = CountDown::first();
            
        if (!$countdown) {
            // Calculate the end date and time for the countdown (5 days from now)
            
            $endDate = Carbon::now()->addDays(7);

            // Calculate the remaining time in days, hours, minutes, and seconds
            $diff = $endDate->diff(Carbon::now());

            // Create a new countdown record in the database
            $countdown = CountDown::create([
                'days' => $diff->days,
                'hours' => $diff->h,
                'minutes' => $diff->i,
                'seconds' => $diff->s,
            ]);
        }

            
            return view('welcome', [
                'candidates' => $candidates,
                'vote' => $vote,
                'countdown' => $countdown
            ]);
    }
    public function update(Request $request)
    {
        $countdown = Countdown::first();

        if ($countdown) {
            $countdown->days = $request->input('days');
            $countdown->hours = $request->input('hours');
            $countdown->minutes = $request->input('minutes');
            $countdown->seconds = $request->input('seconds');
            $countdown->save();
        }

        return response()->json(['success' => true]);
    }
}

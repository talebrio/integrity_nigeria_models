<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\view\view;

class AboutController extends Controller
{
    public function index(){
        
        return view('pages.about');

    }
}

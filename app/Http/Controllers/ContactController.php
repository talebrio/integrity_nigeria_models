<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use Illuminate\Http\Request;
use App\Models\ContactForm;
use Illuminate\Support\Facades\Validator;
use App\Notifications\ContactMessageNotification;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function contact()
    {

        return view('pages.contact');
    }

    public function store(Request $request)
    {



        //Validating incoming request
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'subject' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'message' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }



        //creating an instance of the model
        $contactForm = new ContactForm();

        $contactForm->name = $request->name;
        $contactForm->subject =  $request->subject;
        $contactForm->phone_number = $request->phone;
        $contactForm->email = $request->email;
        $contactForm->message = $request->message;

        $contactForm->save();

        // $contactForm->notify(new ContactMessageNotification($contactForm));

        Mail::to('integrity@integritycameroon.com')->send(new ContactMail($contactForm));


        session()->flash('success', 'Message sent successfully');

        return back();
    }

    public function contactMailPreview($id) {
        $contactData = ContactForm::findOrfail($id);
        return view('pages.mail.contact', [
            'contactData' => $contactData,
        ]);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Candidate extends Model
{
    use HasFactory;

    protected $fillable = [
        'photoUrl',
        'firstName',
        'lastName',
        'number',
        'age',
        'status',
    ];

    public function fullName(): String {
        return $this->firstName.' '.$this->lastName;
    }

    public function photos(): HasMany
    {
        return $this->hasMany(Photo::class);
    }

    public function paiements(): HasMany
    {
        return $this->hasMany(Paiement::class);
    }

    public function votes(): HasMany
    {
        return $this->hasMany(Vote::class);
    }
}

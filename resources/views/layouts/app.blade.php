<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title>Integrity Nigeria - @yield('title')</title>
    <meta charset="utf-8">
    <!--[if IE]>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <![endif]-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="facebook-domain-verification" content="xnn3l7v1bpu8jada41z2sww3uulhr8" />

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

    <!-- Favicons -->
    <link href="/assets/images/logo.png" rel="icon">

    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/animations.css">
    <link rel="stylesheet" href="/assets/css/fonts.css">
    <link rel="stylesheet" href="/assets/css/main.css" class="color-switcher-link">
    <link rel="stylesheet" href="/assets/plugins/aos/aos.css" />
    <link rel="stylesheet" href="/assets/css/style.css">
    <script src="/assets/js/vendor/modernizr-2.6.2.min.js"></script>

    <!--[if lt IE 9]>
  <script src="js/vendor/html5shiv.min.js"></script>
  <script src="js/vendor/respond.min.js"></script>
  <script src="js/vendor/jquery-1.12.4.min.js"></script>
 <![endif]-->
    <link href=
	"https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity=
	"sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>

<body >
    <!--[if lt IE 9]>
  <div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" class="highlight">upgrade your browser</a> to improve your experience.</div>
 <![endif]-->

    <div class="preloader">
        <div class="preloader_image"></div>
    </div>

    <!-- search modal -->
    <div class="modal" tabindex="-1" role="dialog" aria-labelledby="search_modal" id="search_modal">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <i class="rt-icon2-cross2"></i>
            </span>
        </button>
        <div class="widget widget_search">
            <form method="get" class="searchform search-form form-inline" action="./">
                <div class="form-group">
                    <input type="text" value="" name="search" class="form-control"
                        placeholder="Search keyword" id="modal-search-input">
                </div>
                <button type="submit" class="theme_button">Search</button>
            </form>
        </div>
    </div>

    <!-- Unyson messages modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="messages_modal">
        <div class="fw-messages-wrap ls with_padding">
            <!-- Uncomment this UL with LI to show messages in modal popup to your user: -->
            <!--
  <ul class="list-unstyled">
   <li>Message To User</li>
  </ul>
  -->

        </div>
    </div>
    <!-- eof .modal -->

    <!-- wrappers for visual page editor and boxed version of template -->
    <div id="canvas">
        <div id="box_wrapper">

            <!-- template sections -->
            <div class="page_header_wrapper header_darkgrey affix-wrapper" >

                <header class="page_header header_darkgrey header_logo_center">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12 text-md-center">
                                <div class="logo_wrapper">
                                    <a href="/" class="logo top_logo">
                                        <img src="/assets/images/logo.png" alt="">
                                    </a>
                                </div>
                                <!-- header toggler -->
                                <span class="toggle_menu">
                                    <span></span>
                                </span>
                                <!-- main nav start -->
                                <nav class="mainmenu_wrapper">
                                    <ul class="mainmenu sf-menu">
                                        <li class="active mb-5">
                                            <a href="{{ route('home') }}" class="text-decoration-none">Home</a>
                                        </li>
                                        <li class="mb-5">
                                            <a href="{{ route('about') }}" class="text-decoration-none ">About Us</a>
                                        </li>
                                        <li class="mb-5">
                                            <a href="{{ route('candidates') }}"
                                                class="text-decoration-none">Candidates</a>
                                        </li>
                                        <!-- contacts -->
                                        <li class="mb-5">
                                            <a href="{{ route('contact') }}" class="text-decoration-none">Contact</a>
                                        </li>
                                        <!-- eof contacts -->
                                    </ul>
                                </nav>
                                <!-- eof main nav -->
                            </div>
                        </div>
                    </div>
                </header>
            </div>
            @yield('content')


            <footer class="page_footer ds ms section_padding_top_75 section_padding_bottom_65 columns_padding_25">
                <div class="container">
                    <div class="row">

                        <div class="col-md-4">
                            <div class="widget">
                                <h3 class="widget-title">Our Contacts</h3>

                                <div class="media small-teaser">
                                    <div class="media-left fontsize_20">
                                        <i class="fa fa-globe highlight2"></i>
                                    </div>
                                    <div class="media-body fontsize_20">
                                        6221 Ne 82nd Ave,97220 Portland, Oregon, USA
                                    </div>
                                </div>

                                <div class="media small-teaser">
                                    <div class="media-left fontsize_20">
                                        <i class="fa fa-phone highlight2"></i>
                                    </div>
                                    <div class="media-body fontsize_20">
                                        <a href="tel:+19714197088" class="text-decoration-none">+19714197088</a>
                                    </div>
                                </div>

                                <div class="media small-teaser">
                                    <div class="media-left fontsize_20">
                                        <i class="fa fa-envelope highlight2"></i>
                                    </div>
                                    <div class="media-body underlined-links greylinks fontsize_20">
                                        <a
                                            href="mailto:integrity@integritynigeria.com">integrity@integritynigeria.com</a>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6">
                            <div class="widget">
                                <h3 class="widget-title">About Us</h3>
                                <p class="fontsize_20">

                                    Our team is committed to providing exceptional representation for our models,
                                    working with clients
                                    to deliver
                                    exceptional campaigns, fashion shows, and other modelling projects that exceed
                                    expectations.
                                </p>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6">
                            <div class="widget">
                                <h3 class="widget-title">Links</h3>

                                <div class="media small-teaser">

                                    <div class="media-body greylinks fontsize_20">
                                        <a href="{{ route('home') }}">Home</a>
                                    </div>
                                </div>

                                <div class="media small-teaser">

                                    <div class="media-body greylinks fontsize_20">
                                        <a href="{{ route('about') }}">About</a>
                                    </div>
                                </div>

                                <div class="media small-teaser">

                                    <div class="media-body underlined-links greylinks fontsize_20">
                                        <a href="{{ route('candidates') }}">Model</a>
                                    </div>
                                </div>
                                <div class="media small-teaser">

                                    <div class="media-body underlined-links greylinks fontsize_20">
                                        <a href="{{ route('contact') }}">Contact us</a>
                                    </div>
                                </div>

                            </div>

                            <div class="widget">
                                <h3 class="widget-title">Follow Us</h3>
                                <p class="greylinks">
                                    <a class="social-icon border-icon round soc-facebook" href="#"
                                        title="Facebook"></a>
                                    {{-- <a class="social-icon border-icon round soc-twitter" href="#"
                                        title="Twitter"></a>
                                    <a class="social-icon border-icon round soc-google" href="#"
                                        title="Google"></a> --}}
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </footer>

            {{-- <section class="ls page_copyright table_section section_padding_50">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <p class="grey thin">&copy; Copyright 2023 All Rights Reserved</p>
                        </div>
                    </div>
                </div>
            </section> --}}

        </div>
        <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->

    <script src="/assets/plugins/aos/aos.js"></script>
    <script>
        AOS.init({
            duration: 1000,
            // delay: 1000,
            easing: 'ease-in-out'
        });
    </script>

    <script src="/assets/js/compressed.js"></script>
    <script src="/assets/js/main.js"></script>


    <!-- Google Map Script -->
    <script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDTwYSMRGuTsmfl2z_zZDStYqMlKtrybxo"></script>
</body>

</html>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title>Integrity Cameroon </title>
    <meta charset="utf-8">
    <!--[if IE]>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <![endif]-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/animations.css">
    <link rel="stylesheet" href="/assets/css/fonts.css">
    <link rel="stylesheet" href="/assets/css/main.css" class="color-switcher-link">

    <link rel="stylesheet" href="/assets/css/dashboard.css" class="color-switcher-link">
    <script src="/assets/js/vendor/modernizr-2.6.2.min.js"></script>

    <!--[if lt IE 9]>
  <script src="js/vendor/html5shiv.min.js"></script>
  <script src="js/vendor/respond.min.js"></script>
  <script src="js/vendor/jquery-1.12.4.min.js"></script>
 <![endif]-->

</head>

<body class="admin">
    <!--[if lt IE 9]>
  <div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" class="highlight">upgrade your browser</a> to improve your experience.</div>
 <![endif]-->

    <div class="preloader">
        <div class="preloader_image"></div>
    </div>

    <!-- search modal -->
    <div class="modal" tabindex="-1" role="dialog" aria-labelledby="search_modal" id="search_modal">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <i class="rt-icon2-cross2"></i>
            </span>
        </button>
        <div class="widget widget_search">
            <form method="get" class="searchform search-form form-inline" action="./">
                <div class="form-group">
                    <input type="text" value="" name="search" class="form-control"
                        placeholder="Search keyword" id="modal-search-input">
                </div>
                <button type="submit" class="theme_button">Search</button>
            </form>
        </div>
    </div>

    <!-- Unyson messages modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="messages_modal">
        <div class="fw-messages-wrap ls with_padding">
            <!-- Uncomment this UL with LI to show messages in modal popup to your user: -->
            <!--
  <ul class="list-unstyled">
   <li>Message To User</li>
  </ul>
  -->

        </div>
    </div>
    <!-- eof .modal -->

    <!-- Unyson messages modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="admin_contact_modal">
        <!-- <div class="ls with_padding"> -->
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <form class="with_padding contact-form" method="post" action="./">
                    <div class="row">
                        <div class="col-sm-12">
                            <h3>Contact Admin</h3>
                            <div class="contact-form-name">
                                <label for="name">Full Name
                                    <span class="required">*</span>
                                </label>
                                <input type="text" aria-required="true" size="30" value="" name="name"
                                    id="name" class="form-control" placeholder="Full Name">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="contact-form-subject">
                                <label for="subject">Subject
                                    <span class="required">*</span>
                                </label>
                                <input type="text" aria-required="true" size="30" value="" name="subject"
                                    id="subject" class="form-control" placeholder="Subject">
                            </div>
                        </div>

                        <div class="col-sm-12">

                            <div class="contact-form-message">
                                <label for="message">Message</label>
                                <textarea aria-required="true" rows="6" cols="45" name="message" id="message" class="form-control"
                                    placeholder="Message"></textarea>
                            </div>
                        </div>

                        <div class="col-sm-12 text-center">
                            <div class="contact-form-submit">
                                <button type="submit" id="contact_form_submit" name="contact_submit"
                                    class="theme_button wide_button color1">Send Message</button>
                                <button type="reset" id="contact_form_reset" name="contact_reset"
                                    class="theme_button wide_button">Clear Form</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- eof .modal -->

    <!-- wrappers for visual page editor and boxed version of template -->
    <div id="canvas">
        <div id="box_wrapper">

            <!-- template sections -->

            <header class="page_header_side page_header_side_sticked active-slide-side-header ds">
                <div class="side_header_logo ds ms">
                    <a href="{{ route('home') }}">
                        <span class="logo_text playfair">
                            Intergrity
                            <strong>Cameroon</strong>
                        </span>
                    </a>
                </div>
                <span class="toggle_menu_side toggler_light header-slide">
                    <span></span>
                </span>
                <div class="scrollbar-macosx">
                    <div class="side_header_inner">

                        <!-- user -->

                        <div class="user-menu">


                            <ul class="menu-click">
                                <li>
                                    <a href="#">
                                        <div class="media">
                                            <div class="media-left media-middle">
                                                <img src="images/team_square/01.jpg" alt="">
                                            </div>
                                            <div class="media-body media-middle">
                                                <h4>Agendia Brandon</h4>
                                                Administrator

                                            </div>

                                        </div>
                                    </a>
                                    <ul>
                                        <li>
                                            <form action="{{ route('logout') }}" method="POST">
                                                @csrf
                                                <i class="fa fa-sign-out"></i>
                                                <button type="submit" style="border:none;color:#ffffff;font-size:1.5rem;">Logout</button>
                                            </form>
                                            {{-- <a href="{{ route('logout') }}">
                                                <i class="fa fa-sign-out"></i>
                                                Log Out
                                            </a> --}}
                                        </li>
                                    </ul>
                                </li>
                            </ul>

                        </div>

                        <!-- main side nav start -->
                        <nav class="mainmenu_side_wrapper">
                            <h3 class="dark_bg_color">Dashboard</h3>
                            <ul class="menu-click">
                                <li>
                                    <a href="{{ route('dashboard.index') }}">
                                        <i class="fa fa-th-large"></i>
                                        Dashboard
                                    </a>

                                </li>
                            </ul>

                            <h3 class="dark_bg_color">Pages</h3>
                            <ul class="menu-click">
                               
                                <li>
                                    <a href="{{ route('dashboard.show') }}">
                                        <i class="fa fa-file-text"></i>
                                        Candidate
                                    </a>
                                    <ul>
                                        <li>
                                            <a href="{{ route('dashboard.show') }}">
                                                All Candidate
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('dashboard.create') }}">
                                                Add Candidate
                                            </a>
                                        </li>

                                    </ul>
                                </li>
                                   
                            
                            </ul>
                        </nav>
                        <!-- eof main side nav -->


                    </div>
                </div>
            </header>

            <header class="page_header header_darkgrey">

                <div class="widget widget_search">
                    <form method="get" class="searchform form-inline" action="./">
                        <div class="form-group">
                            <label class="screen-reader-text" for="widget-search-header">Search for:</label>
                            <input id="widget-search-header" type="text" value="" name="search"
                                class="form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="theme_button">Search</button>
                    </form>
                </div>

                <!-- eof .header_right_buttons -->
            </header>

           
            @yield('content')

            <section class="page_copyright ds darkblue_bg_color">
				<div class="container-fluid">
					<div class="row">
						<div class="col-sm-6">
							<p class="grey">&copy; Copyrights 2023</p>
						</div>
					</div>
				</div>
			</section>

            <a class="side-button side-contact-button" data-target="#admin_contact_modal" href="#admin_contact_modal"
            data-toggle="modal" role="button">
            <i class="fa fa-envelope"></i>
        </a>
    
    
        <!-- template init -->
        <script src="/assets/js/compressed.js"></script>
        <script src="/assets/js/main.js"></script>
    
        <!-- dashboard libs -->
    
        <!-- events calendar -->
        <script src="/assets/js/admin/moment.min.js"></script>
        <script src="/assets/js/admin/fullcalendar.min.js"></script>
        <!-- range picker -->
        <script src="/assets/js/admin/daterangepicker.js"></script>
    
        <!-- charts -->
        <script src="/assets/js/admin/Chart.bundle.min.js"></script>
        <!-- vector map -->
        <script src="/assets/js/admin/jquery-jvectormap-2.0.3.min.js"></script>
        <script src="/assets/js/admin/jquery-jvectormap-world-mill.js"></script>
        <!-- small charts -->
        <script src="/assets/js/admin/jquery.sparkline.min.js"></script>
    
        <!-- dashboard init -->
        <script src="/assets/js/admin.js"></script>
    
    </body>
    
    </html>
    
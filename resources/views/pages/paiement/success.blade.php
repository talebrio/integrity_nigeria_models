@extends('layouts.app')

@section('title', 'Paiement success')

@section('content')
<style>
    .container {
        text-align: center;
    }
    .title {
        font-size: 50px;
        color: #4caf50;
    }
    .teaser_icon {
        font-size: 150px;
        color: #4caf50;
    }
    a{
        text-decoration: none;
    }
</style>

<div class="container">
    <h1 class="title">Voted successfully !</h1>
    <div class="success-animation">
        <div class="teaser_icon size_normal">
            <i class="rt-icon2-checkmark"></i>
        </div>
    </div>
    <h1 style="font-size: 20;">Thank you for voting. <br> Your transaction has been successfully processed.</h1>
    <a class="theme_button btn-properties mt-5" href="/">Back to home page</a>

</div>

@endsection

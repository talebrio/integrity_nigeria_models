@extends('layouts.app')

@section('title', 'Paiement success')

@section('content')
<style>
    .container {
        text-align: center;
    }
    .title {
        font-size: 50px;
        color: #ef0d0d;
    }
    .teaser_icon {
        font-size: 150px;
        color: #ef0d0d;
    }
    a{
        text-decoration: none;
    }
</style>

<div class="container">
    <h1 class="title">Payment Cancel!</h1>
    <div class="success-animation">
        <div class="teaser_icon size_normal">
            <img src="{{ URL::TO('assets/images/cancel_594864.png') }}" alt="" style="width: 200px; height: 200px;"/>

        </div>
    </div>
    <h1 style="font-size: 20;">Your Payment Was Cancel!</h1>
    <a class="theme_button btn-properties mt-5" href="/">Back to home page</a>

</div>

@endsection

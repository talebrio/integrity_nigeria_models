@extends('layouts.app')

@section('title', 'Welcome')

@section('content')

    {{-- <section class="page_breadcrumbs transp_gradient_bg section_padding_75">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h2>About</h2>
                    <ol class="breadcrumb ">
                        <li>
                            <a href="{{ route('home') }}">
                                Home
                            </a>
                        </li>

                        <li class="active">
                            <a href="{{ route('candidates') }}">
                                Models
                            </a>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </section> --}}
    <section class="ds ms page_about parallax section_padding_100 columns_padding_25 columns_margin_bottom_30">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center mt-5">
                    <h2 class="section_header">Story of Our Agency</h2>
                    {{-- <p class="small-text highlight lusitana">for a chance to join us</p> --}}
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <p class="fontsize_20">
                        Welcome to Integrity Nigeria, the leading modeling agency in Nigeria. Our agency started in Cameroon, 
                        with its first ever successful project in 2023 and it’s dedicated to discovering, nurturing, and promoting 
                        aspiring models of all ages and backgrounds in Nigeria and beyond. At Integrity Nigeria, we are passionate 
                        about helping models achieve their dreams of becoming successful in the modeling industry. We provide a 
                        platform for young and talented models, both male and female, to showcase their skills and talents in 
                        various modeling events, competitions, and campaigns.
                        <br>
                        Our agency is committed to excellence in everything we do, from scouting new talents to providing top-quality 
                        training and support to our models. We believe in promoting integrity, professionalism, and hard work, and we 
                        strive to instill these values in all our models. With regular competitions and events, our models have numerous
                         opportunities to showcase their skills and gain exposure to industry professionals and the public. We also 
                         offer our models access to top photographers, designers, and fashion experts to help them build their 
                         professional portfolios.

                        <br>
                        Whether you're an aspiring model looking for your big break or an established model seeking new
                         opportunities, we welcome you to join us at Integrity Nigeria. Together, we can achieve great 
                         things and take the modeling industry to new heights.


                    </p>

                </div>

            </div>
        </div>
    </section>
    <section class="ds ms section_padding_100">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h2 class="section_header">Team Leader</h2>
                    {{-- <p class="small-text highlight lusitana">A Team of Professionals</p> --}}
                </div>
            </div>
            <div class="row topmargin_40">
                <div class="col-sm-12">
                    <div class="owl-carousel" data-responsive-lg="3" data-margin="60" data-nav="false" data-dots="false">
                        <div class="vertical-item content-padding text-center">
                            <div class="item-media">
                                <img src="{{ URL::TO('assets/images/agendia.jpeg') }}" alt="" />
                            </div>
                            <div class="item-content">
                                <h4 class="margin_0">
                                    <h3>Agendia Brandon</h3>
                                </h4>
                                <p class="small-text lusitana highlight">CEO</p>
                            </div>
                        </div>



                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

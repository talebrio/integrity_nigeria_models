@extends('layouts.app')

@section('title', 'Welcome')

@section('content')

    <style>
        .form-control {
            text-transform: none !important;
        }
    </style>

    {{-- <section class="page_breadcrumbs transp_gradient_bg section_padding_75">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h2>CONTACT</h2>
                    <ol class="breadcrumb greylinks">
                        <li>
                            <a href="{{ route('home') }}">
                                Home
                            </a>
                        </li>

                        <li class="active">
                            <a href="{{ route('contact') }}">
                                Contact
                            </a>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </section> --}}

    <section class="ds columns_padding_25 section_padding_top_75 section_padding_bottom_75 columns_margin_bottom_30"
    style="background-image: url('{{ URL::TO('assets/images/about4.jpg') }}'); background-attachment:fixed;">
        <div class="container">
            {{-- <div class="row">

                <div class="col-md-8 to_animate mx-auto mt-3" data-animation="scaleAppear" >
                    @if (session('success'))
                        <div class="alert alert-success">{{ session('success') }}</div>
                    @endif
                    <h1 class="text-center">Contact Us</h1>
                    <form class="contact-form row columns_padding_10 columns_margin_bottom_10"
                        action="{{ route('contact.store') }}" method="POST">
                        @csrf
                        <div class="col-sm-6">
                            <div class="form-group margin_0">
                                <label for="name">Full Name
                                    <span class="required">*</span>
                                </label>
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <input type="text" aria-required="true" size="30" value="" name="name"
                                    id="name" class="form-control" placeholder="Name">
                                @error('name')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group margin_0">
                                <label for="subject">Subject
                                    <span class="required">*</span>
                                </label>
                                <i class="fa fa-comment" aria-hidden="true"></i>
                                <input type="text" aria-required="true" size="30" value="" name="subject"
                                    id="subject" class="form-control" placeholder="Subject">
                                @error('subject')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group margin_0">
                                <label for="phone">Phone
                                    <span class="required">*</span>
                                </label>
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <input type="text" aria-required="true" size="30" value="" name="phone"
                                    id="phone" class="form-control" placeholder="Phone number">
                                @error('phone')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group margin_0">
                                <label for="email">Email address
                                    <span class="required">*</span>
                                </label>
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                <input type="email" aria-required="true" size="30" value="" name="email"
                                    id="email" class="form-control" placeholder="Email address">
                                @error('email')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group margin_0">
                                <label for="message">Message</label>
                                <i class="fa fa-folder-open" aria-hidden="true"></i>
                                <textarea aria-required="true" rows="9" cols="45" name="message" id="message" class="form-control"
                                    placeholder="Message..."></textarea>
                                @error('message')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="contact-form-submit topmargin_10">
                                <button type="submit" id="contact_form_submit" name="contact_submit"
                                    class="theme_button inverse">Submit</button>
                                <button type="reset" id="contact_form_reset" name="contact_reset"
                                    class="theme_button color2 inverse">Clear</button>
                            </div>
                        </div>
                    </form>

                </div>

            </div> --}}
            <!--.col-* -->

            <div class="col-md-4 to_animate" data-animation="scaleAppear">

                <h2>Contact Info</h2>

                <div class="media small-teaser">
                    <div class="media-left">
                        <i class="fa fa-globe highlight2"></i>
                    </div>
                    <div class="media-body">
                        <h3>6221 Ne 82nd Ave. 97220 portland, Oregon, USA</h3>
                    </div>
                </div>

                <div class="media small-teaser">
                    <div class="media-left">
                        <i class="fa fa-phone highlight2"></i>
                    </div>
                    <div class="media-body">
                        <h3>1-971-419-7088</h3>
                    </div>
                </div>

                <div class="media small-teaser">
                    <div class="media-left">
                        <i class="fa fa-envelope highlight2"></i>
                    </div>
                    <div class="media-body underlined-links greylinks">
                        <h3>  <a href="mailto:integrity@integritynigeria.com">integrity@integritynigeria.com</a>
                        </h3>
                    </div>
                </div>

            </div>
            <!--.col-* -->

        </div>
        <!--.row -->

        </div>
        <!--.container -->

    </section>

@endsection

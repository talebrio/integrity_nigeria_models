<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mail de contact</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
        }

        .container {
            max-width: 500px;
            margin: 0 auto;
            padding: 20px;
        }

        h1 {
            text-align: center;
        }

        .email-details {
            margin-top: 20px;
            border: 1px solid #ccc;
            padding: 10px;
            border-radius: 4px;
        }

        .email-details label {
            font-weight: bold;
        }

        .email-details p {
            margin: 5px 0;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Contact Mail</h1>
        <div class="email-details">
            <label>Name :</label>
            <p>{{ $contactData->name }}</p>

            <label>E-mail adress :</label>
            <p>{{ $contactData->email }}</p>

            <label>Phone number :</label>
            <p>{{ $contactData->phone_number }}</p>

            <label>Subject :</label>
            <p>{{ $contactData->subject }}</p>

            <label>Message :</label>
            <p>{{ $contactData->message }}</p>
        </div>
    </div>
</body>
</html>

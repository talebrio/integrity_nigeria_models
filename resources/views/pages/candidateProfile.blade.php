@extends('layouts.app')

@section('title', "$candidate->firstName $candidate->lastName")

@section('content')
    {{-- <section class="page_breadcrumbs section_padding_75 transp_gradient_bg ">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h2> {{ $candidate->firstName }} {{ $candidate->lastName }} </h2>
                </div>
            </div>
        </div>
    </section> --}}

    {{-- <section class="product p-5">
        <div class="pu0-grid pu0-gap-md">
            <div class="pu0-col-6@md pu0-col-4@lg">
                <figure class="image-zoom js-image-zoom ">
                    <img class="image-zoom__preview js-image-zoom__preview" src="{{ $candidate->photoUrl }}"
                        alt="Preview image description">
                    <figcaption class="pu0-sr-only">Image Caption</figcaption>
                </figure>
            </div>

            <div class="pu0-col-6@md pu0-col-5@lg">
                <div class="pu0-margin-bottom-xs">
                    <h1 class="text-center">{{ $candidate->firstName }} {{ $candidate->lastName }}</h1>
                </div>

                <div class="pu0-margin-bottom-xs">
                    <h3 class="text-center"><b>Age</b> {{ $candidate->age }}</h3>
                </div>

                <div class="pu0-margin-bottom-xs">
                    <h3 class="text-center"><b>No</b> {{ $candidate->number }}</h3>
                </div>


                <div class="pu0-text-component pu0-text-gap-md pu0-margin-y-md fontsize_16">
                    <p class="pu0-text-md fontsize_20"> <span class="fontsize_30"><b>{{ $nbVote }}</b></span> Votes</p>
                </div>

                <div class="pu0-margin-bottom-md">
                    <form action="">
                        <div class="pu0-margin-bottom-xs xaf">
                            <p class="text-center"><span class="vote">1</span> VOTE = <span class="frs">100</span> XAF
                            </p>
                        </div>

                        <select name="" id="currency_opt">
                            <option value="XAF">Central African CFA franc</option>

                        </select>

                    </form>

                </div>


                <form method="POST" action="{{route('vote', ['number'=> $candidate->number])}}">
                    @csrf
                    <div class="pu0-flex pu0-gap-xs">

                        <div class="number-input number-input--v1 js-number-input ">
                            <button type="button" class="number-input__btn number-input__btn--minus js-number-input__btn btn-decrease"
                                aria-label="Decrease Number">-</button>
                            <input class="pu0-form-control js-number-input__value text-number" type="number" name="qtyInput"
                                id="qtyInput" min="0" max="10" step="1" value="1">

                            <button type="button" class="number-input__btn number-input__btn--plus js-number-input__btn btn-increase"
                                aria-label="Increase Number">+</button>

                        </div>

                        <button type="submit" class="pu0-btn pu0-btn--primary">Vote Now</button>
                    </div>
                </form>
                <div class="widget mt-6 p-4">
                    <h5 class="">Get a free vote by sharing on facebook</h5>
                    <p class="greylinks">
                        <a class="social-icon border-icon round soc-facebook" href="#" title="Facebook"></a>
                    </p>
                </div>
            </div>
        </div>
    </section> --}}

    <section class="ds section_padding_100 columns_padding_25">
        <div class="container">
            <div class="row">
                <div class="col-sm-6" data-aos="fade-up">
                    <div class="vertical-item content-padding text-center">
                        <div class="item-media" style="border-radius: 10px 10px 0px 0px;">
                            <img class="zoom-on-hover" src="{{ $candidate->photoUrl }}" alt="" />
                        </div>
                        <div class="item-content transp_gradient_bg">
                            <h3 class="model-name">
                                <a href="{{ route('candidate.show', $candidate->number) }}">{{ $candidate->firstName }}
                                    {{ $candidate->lastName }}</a>
                            </h3>
                            <p class="small-text lusitana highlight">Top model</p>

                            <div class="fontsize_16 lusitana text-uppercase grey parameters text-center">
                                <span class="m-3">
                                    No
                                    <br>
                                    <span class="highlight">{{ $candidate->number }}</span>
                                </span>
                                <span class="m-3">
                                    Age
                                    <br>
                                    <span class="highlight">{{ $candidate->age }}</span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6" data-aos="fade-up">
                    <div class="">
                        <div class="teaser main_bg_color2 font-icon-background-teaser inner-border text-center">
                            <h3 class="counter highlight counted" data-from="0" data-to="{{ $nbVote }}"
                                data-speed="1500">{{ $nbVote }}</h3>
                            <h3>{{ $nbVote > 1 ? 'Votes' : 'Vote' }}</h3>
                            <i class="fa fa-heart icon-background"></i>
                        </div>
                        {{-- <div class="teaser main_bg_color2 counter-background-teaser inner-border text-center">
                            <span class="counter counter-background counted" data-from="0" data-to="{{$nbVote}}" data-speed="2100">{{$nbVote}}</span>
                            <h3 class="counter highlight counted" data-from="0" data-to="346" data-speed="1500">{{$nbVote}}</h3>
                            <h3>{{$nbVote > 1 ? 'Votes': 'Vote'}}</h3>
                        </div> --}}

                    </div>
                    <form method="POST" id="voteForm" action="{{ route('vote', ['number' => $candidate->number]) }}">
                        @csrf
                        <h3 class="mt-4"><span class="vote-quantity">1</span> VOTE = <span
                                class="vote-total-price">50</span></s> NGN</h3>
                        <h6>Vote quantity</h6><br>
                        <span class="quantity form-group">
                            <input type="button" value="-" class="btn-decrease">
                            <input type="number" step="1" min="0" name="product_quantity" value="1"
                                title="Quantity" id="product_quantity" class="form-control text-number">
                            <input type="button" value="+" class="btn-increase">
                        </span>
                        <button type="submit" class="theme_button main_bg_color2 mt-4"
                            style="background-color: #c25582; border: 1px solid #c25582">VOTE
                            NOW</button>
                    </form>
                    {{-- <div class="apsc-icons-wrapper clearfix apsc-theme-4">
                        <div class="apsc-each-profile">
                            <a class="apsc-facebook-icon clearfix" style="width: fit-content;" href="#">
                                <div class="apsc-inner-block">
                                    <span class="social-icon">
                                        <i class="fa fa-facebook apsc-facebook"></i>
                                        <span class="media-name">Get a free vote with Facebook</span>
                                    </span>
                                </div>
                            </a>
                        </div>

                    </div> --}}


                    <div class="socials mt-5">
                        <a href="#" class="social-icon soc-facebook highlight2"></a>
                        <a href="#" class="social-icon soc-instagram highlight2"></a>
                        <a href="#" class="social-icon soc-twitter highlight2"></a>
                    </div>

                    {{-- <div class="media small-teaser grey">
                        <div class="media-left">
                            <i class="fa fa-globe highlight2"></i>
                        </div>
                        <div class="media-body">
                            Jacksotts str. 698 San Diego, California, USA
                        </div>
                    </div>

                    <div class="media small-teaser grey">
                        <div class="media-left">
                            <i class="fa fa-phone highlight2"></i>
                        </div>
                        <div class="media-body">
                            1-800-123-4567, 1-800-123-4568
                        </div>
                    </div>

                    <div class="media small-teaser grey">
                        <div class="media-left">
                            <i class="fa fa-envelope highlight2"></i>
                        </div>
                        <div class="media-body underlined-links">
                            <a href="mailto:your@mail.com">rachel_tate@support.com</a>
                        </div>
                    </div> --}}

                    <!-- eof .isotope_container.row -->
                </div>
            </div>
        </div>
    </section>


    <script>
        document.addEventListener('DOMContentLoaded', function() {

            let reducebtn = document.querySelector('.btn-decrease');
            let increasebtn = document.querySelector('.btn-increase');
            let valuebtn = document.querySelector('.text-number')
            // let valueInput = parseInt(valuebtn.value)
            let voteTotalPrice = document.querySelector('.vote-total-price');
            let voteQuantity = document.querySelector('.vote-quantity');

            let voteForm = document.querySelector('#voteForm')

            valuebtn.addEventListener('input', () => {
                voteQuantity.textContent = valuebtn.value
                voteTotalPrice.textContent = valuebtn.value * 50
            })

            reducebtn.addEventListener("click", () => {

                if (valuebtn.value > 1) {
                    valuebtn.value--;
                    valuebtn.value = valuebtn.value;
                    voteTotalPrice.textContent = parseInt(voteTotalPrice.textContent) - 50;
                    voteQuantity.textContent = valuebtn.value
                } else {
                    valuebtn.value = 1;
                }

            })

            increasebtn.addEventListener("click", () => {
                valuebtn.value++;
                valuebtn.value = valuebtn.value;
                voteTotalPrice.textContent = parseInt(voteTotalPrice.textContent) + 50;
                voteQuantity.textContent = valuebtn.value

            })

            voteForm.addEventListener('submit', function(event) {
                if (valuebtn.value == '')
                    event.preventDefault();
            })
        });
    </script>

@endsection

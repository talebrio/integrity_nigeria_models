@extends('layouts.app')

@section('title', 'Welcome')

@section('content')

{{-- <section class="page_breadcrumbs transp_gradient_bg section_padding_75">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h2>    CANDIDATES</h2>
                <ol class="breadcrumb ">
                    <li  >
                        <a href="{{ route('home') }}" >
                            Home
                        </a>
                    </li>

                </ol>
            </div>
        </div>
    </div>
</section> --}}

<section class="ds columns_padding_25 section_padding_top_75 section_padding_bottom_75 columns_margin_bottom_30"
style="background-image: url('{{ URL::TO('assets/images/about4.jpg') }}'); background-attachment:fixed;">

<div class="container">
    <div class="row p-4">
     @foreach ($candidates as $key => $candidate)

        <div class="col-sm-4" data-aos="fade-up">
            <a href="{{ route('candidate.show', $candidate->number) }}">
              <div class="vertical-item content-padding text-center">
                  <div class="item-media" style="border-radius: 10px 10px 0px 0px;">
                      <img class="zoom-on-hover candidate-item" src="{{ $candidate->photoUrl }}"  alt="" />
                  </div>
                  <div class="item-content transp_gradient_bg">
                      <h3 class="model-name">
                          <a href="{{ route('candidate.show', $candidate->number) }}">{{ $candidate->firstName }} {{ $candidate->lastName }}</a>
                      </h3>
                      <p class="small-text lusitana highlight">Top model</p>

                      <div class="fontsize_16 lusitana text-uppercase grey parameters">
                          <span>
                              No
                              <br>
                              <span class="highlight">{{ $candidate->number }}</span>
                          </span>
                          <span>
                              Age
                              <br>
                              <span class="highlight">{{ $candidate->age }}</span>
                          </span>
                              <span>
                                  Vote
                                  <br>
                                  <span class="highlight">{{ $vote[$candidate->id] }}</span>
                              </span>
                      </div>
                  </div>
              </div>
            </a>
          </div>
    @endforeach

    </div>
</div>
</section>
@endsection

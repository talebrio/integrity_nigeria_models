@extends('layouts.app')

@section('title', 'Welcome')

@section('content')
    <section class="intro_section page_mainslider ds">
        <!-- Modal -->
        <div id="myModal" class="modal">
            <!-- Modal content -->
            <div class="modal-content">
                <span class="close">&times;</span>
                <span class="voting-text">VOTING END IN</span>
                <div id="countdown">
                    <div class="square-box">
                        <span id="days">{{ $countdown->days }}</span>
                    </div>
                    <div class="square-box">
                        <span id="hours">{{ $countdown->hours }}</span>
                    </div>
                    <div class="square-box">
                        <span id="minutes">{{ $countdown->minutes }}</span>
                    </div>
                    <div class="square-box">
                        <span id="seconds">{{ $countdown->seconds }}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="flexslider" data-nav="false">
            <ul class="slides">

                <li class="image">
                    <img src="{{ URL::TO('assets/images/website.jpg') }}" alt="">

                    <div
                        class="bottom-content main_bg_color3 transparent section_padding_top_30 section_padding_bottom_30 table_section">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-4">
                                    <h2 class="section_header">
                                        Agendia Brandon
                                    </h2>
                                    <p class="small-text lusitana highlight bottommargin_0">
                                        Top Model &amp; director
                                    </p>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 text-right">
                                <div class="slide_description_wrapper" data-aos="fade-left">
                                    <div class="slide_description text-center">
                                        <div class="intro-layer" data-animation="fadeInUp">
                                            <h3 class="text-uppercase ">
                                                welcome to
                                            </h3>
                                        </div>
                                        <div class="intro-layer" data-animation="fadeInUp">
                                            <h3 class="text-uppercase">
                                                miss
                                            </h3>
                                        </div>
                                        <div class="intro-layer" data-animation="fadeInUp">
                                            <h2 class="text-uppercase big">
                                                INTEGRITY
                                                <br> NIGERIA
                                            </h2>
                                        </div>
                                        <div class="intro-layer inline-content" data-animation="fadeInUp">
                                            {{-- <a class="theme_button color2 active
                                            text-decoration-none btn-primary" href="{{ route('about') }}">About us</a> --}}
                                            <a class="theme_button inverse
                                            text-decoration-none btn-properties btn-lg active "
                                                href="{{ route('candidates') }}">Vote Now</a>
                                        </div>
                                    </div>
                                    <!-- eof .slide_description -->
                                </div>
                                <!-- eof .slide_description_wrapper -->
                            </div>
                            <!-- eof .col-* -->
                        </div>
                        <!-- eof .row -->
                    </div>
                    <!-- eof .container -->
                </li>

            </ul>
        </div>
        <!-- eof flexslider -->
    </section>

    <section id="models" class="ds ms section_padding_top_50 container_padding_60 bottompadding_10"
        style="background-image: url('{{ URL::TO('assets/images/about4.jpg') }}'); background-attachment:fixed;">
        <div class="container">
            <div class="row">
                <div class="col-md-8" data-aos="fade-right">
                    <p class="fontsize_20 header_darkgrey" style="border-radius: 20px">
                        Integrity Nigeria is a premium model agency dedicated to representing top-quality fashion models
                        across Nigeria.
                        Our team is committed to providing exceptional representation for our models, working with clients
                        to deliver
                        exceptional campaigns, fashion shows, and other modelling projects that exceed expectations. With
                        our high
                        standards for professionalism and excellence,
                        Integrity Nigeria is a trusted partner for clients and a sought-after agency for aspiring models.
                    </p>
                    <div class="col-sm-12 text-center">
                        <a href="{{ route('about') }}"
                            class="theme_button btn-properties
                    text-decoration-none">Read more</a>
                    </div>
                </div>
                <div class="col-md-4" data-aos="fade-left">
                    <img class="rounded-image" src="{{ URL::TO('assets/images/ceo.jpeg') }}" alt="">
                </div>
            </div>
        </div>
    </section>
    {{-- MODELS SEC --}}
    <section id="models" class="ds ms section_padding_top_50 container_padding_60 bottompadding_10"
        style="background-image: url('{{ URL::TO('assets/images/about4.jpg') }}'); background-attachment:fixed;">
        <div class="container-fluid">
            <div class="row">
                <div class="intro-layer" data-animation="fadeInUp">
                    <h2 class="text-uppercase big text-center text-size mb-4">
                        All models
                    </h2>
                </div>
                {{-- <div class="col-md-7 col-md-pull-5 mt-5">
                    <div class="filters carousel_filters">
                        <a href="#" class="theme_button inverse selected
                        text-decoration-none" data-filter="*">All</a>
                        <a href="#" class="theme_button inverse" data-filter=".fashion">Men</a>
                    <a href="#" class="theme_button inverse" data-filter=".studio">Women</a>

                    </div>
                </div> --}}
            </div>
        </div>


        <div class="container">
            <div class="row p-4">
                @foreach ($candidates as $key => $candidate)
                    <div class="col-sm-4" data-aos="fade-up">
                        <a href="{{ route('candidate.show', $candidate->number) }}">
                            <div class="vertical-item content-padding text-center">
                                <div class="item-media" style="border-radius: 10px 10px 0px 0px;">
                                    <img class="zoom-on-hover candidate-item" src="{{ $candidate->photoUrl }}"
                                        alt="" />
                                </div>
                                <div class="item-content transp_gradient_bg">
                                    <h3 class="model-name">
                                        <a href="{{ route('candidate.show', $candidate->number) }}">{{ $candidate->firstName }}
                                            {{ $candidate->lastName }}</a>
                                    </h3>
                                    <p class="small-text lusitana highlight">Model</p>

                                    <div class="fontsize_16 lusitana text-uppercase grey parameters ">
                                        <span class="m-3 m-md-3">
                                            No
                                            <br>
                                            <span class="highlight">{{ $candidate->number }}</span>
                                        </span>
                                        <span class="m-2 m-md-3">
                                            Age
                                            <br>
                                            <span class="highlight">{{ $candidate->age }}</span>
                                        </span>
                                        <span class="m-2 m-md-3">
                                            Vote
                                            <br>
                                            <span class="highlight">{{ $vote[$candidate->id] }}</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach

            </div>
        </div>
        <script>
            var modal = document.getElementById("myModal")
            // Function to open the countdown modal
            function openCountdownModal() {
                document.getElementById("myModal").style.display = "block";
            }
            openCountdownModal()

            // Function to close the countdown modal
            function closeCountdownModal() {
                document.getElementById("myModal").style.display = "none";
            }

            // Get the <span> element that closes the modal
            var span = document.querySelector(".modal-content .close");

            span.addEventListener("click", () => {
                modal.style.display = "none";
            })

            document.getElementById("myModal").addEventListener("click",()=>{
                modal.style.display = "none";
            })

            // Function to update the countdown values in the database
            function updateCountdownValues(days, hours, minutes, seconds) {
                // Create a new FormData object and append the countdown values
                const formData = new FormData();
                formData.append('_token', '{{ csrf_token() }}');
                formData.append('days', days);
                formData.append('hours', hours);
                formData.append('minutes', minutes);
                formData.append('seconds', seconds);

                // Send a fetch request to update the countdown values
                fetch('{{ route('countdown.update') }}', {
                        method: 'POST',
                        body: formData
                    })
                    .catch(error => {
                        console.error('Failed to update countdown values:', error);
                    });
            }


            // Function to update the countdown UI
            function updateCountdown() {
                // Get the current countdown values from the UI
                var daysElement = document.getElementById('days');
                var hoursElement = document.getElementById('hours');
                var minutesElement = document.getElementById('minutes');
                var secondsElement = document.getElementById('seconds');

                var days = parseInt(daysElement.textContent);
                var hours = parseInt(hoursElement.textContent);
                var minutes = parseInt(minutesElement.textContent);
                var seconds = parseInt(secondsElement.textContent);


                // Decrement the countdown values
                if (seconds > 0) {
                    seconds--;
                } else {
                    seconds = 59;
                    if (minutes > 0) {
                        minutes--;
                    } else {
                        minutes = 59;
                        if (hours > 0) {
                            hours--;
                        } else {
                            hours = 23;
                            if (days > 0) {
                                days--;
                            } else {
                                // Countdown has ended
                                closeCountdownModal();
                                return;
                            }
                        }
                    }
                }
                // Update the countdown elements in the UI
                daysElement.textContent = days + "d";
                hoursElement.textContent = hours + "h";
                minutesElement.textContent = minutes + "m";
                secondsElement.textContent = seconds + "s";

                // Update the countdown values in the database
                updateCountdownValues(days, hours, minutes, seconds);

                // Schedule the next update after 1 second
                setTimeout(updateCountdown, 1000);
            }


            // Start the countdown when the page loads
            document.addEventListener('DOMContentLoaded', updateCountdown);
        </script>

    </section>

@endsection

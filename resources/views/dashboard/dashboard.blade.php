@extends('layouts.dashboard')

    @section('title', 'Welcome') 

<style>
    table a i{
         font-size: 22px;
    }
</style>

@section('content')
    <section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
        <section class="ds with_bottom_border">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <ol class="breadcrumb darklinks">
                            <li>
                                <a href="{{ route('dashboard.index') }}">Homepage</a>
                            </li>
                            <li class="active">Dashboard</li>
                        </ol>
                    </div>
                    <!-- .col-* -->
                    <div class="col-md-6 text-md-right">
                        <span class="dashboard-daterangepicker">
                            <i class="fa fa-calendar"></i>
                            <span></span>
                            <i class="caret"></i>
                        </span>
                    </div>
                    <!-- .col-* -->
                </div>
                <!-- .row -->
            </div>
            <!-- .container -->
        </section>
        <div class="container-fluid">


            <div class="row">
                <div class="col-md-4">
                    <h3 class="dashboard-page-title">Dashboard
                    </h3>
                </div>

            </div>
            <!-- .row -->



            <div class="row">
                <div class="col-lg-4 col-sm-6">

                    <div class="teaser warning_bg_color counter-background-teaser text-center">
                        <span class="counter counter-background" data-from="0" data-to="{{ $totalCandidates }}"
                            data-speed="2100">0</span>
                        <h3 class="counter highlight" data-from="0" data-to="{{ $totalCandidates }}" data-speed="2100">0
                        </h3>
                        <p>Total Candidates</p>
                    </div>

                </div>

                <div class="col-lg-4 col-sm-6">

                    <div class="teaser danger_bg_color counter-background-teaser text-center">
                        <span class="counter counter-background" data-from="0" data-to="{{ $totalVotes }}"
                            data-speed="1500">0</span>
                        <h3 class="counter highlight" data-from="0" data-to="{{ $totalVotes }}" data-speed="1500">0</h3>
                        <p>Total Votes</p>
                    </div>

                </div>

                <div class="col-lg-4 col-sm-6">

                    <div class="teaser info_bg_color counter-background-teaser text-center">
                        <span class="counter counter-background" data-from="0" data-to="{{ $totalAmount }}" data-speed="1800">{{ $totalAmount }}</span>
                        <h3 class="counter-wrap highlight" data-from="0" data-to="{{ $totalAmount }}" data-speed="1800">
                            <span class="counter" data-from="0" data-to="{{ $totalAmount }}" data-speed="1500">{{ $totalAmount }}</span>
                            <small class="counter-add">NGN</small>
                        </h3>
                        <p>Total Profit</p>
                    </div>

                </div>
            </div>

            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <h3>All Candidates</h3>
                    </div>
                    <!-- .col-* -->
                </div>
                <!-- .row -->

                <div class="row">
                    <div class="col-xs-12">
                        <div class="with_border with_padding">

                            <div class="row admin-table-filters">
                                <div class="col-lg-9">

                                    <form action="./" class="form-inline filters-form">
                                        <span>
                                            <label class="grey" for="with-selected">With Selected:</label>
                                            <select class="form-control with-selected" name="with-selected"
                                                id="with-selected">
                                                <option value="">-</option>
                                                <option value="approve">Update</option>
                                                <option value="delete">Delete</option>
                                            </select>
                                        </span>
                                        <span>
                                            <label class="grey" for="orderby">Sort By:</label>
                                            <select class="form-control orderby" name="orderby" id="orderby">
                                                <option value="date" selected>Date</option>
                                                <option value="author">Age</option>
                                                <option value="title">Candidate No</option>
                                                <option value="status">age</option>
                                            </select>
                                        </span>

                                        <span>
                                            <label class="grey" for="showcount">Show:</label>
                                            <select class="form-control showcount" name="showcount" id="showcount">
                                                <option value="10" selected>10</option>
                                                <option value="20">20</option>
                                                <option value="30">30</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                            </select>
                                        </span>
                                    </form>

                                </div>
                                <!-- .col-* -->
                                <div class="col-lg-3 text-lg-right">
                                    <div class="widget widget_search">

                                        <form method="get" class="searchform form-inline" action="./">
                                            <div class="form-group">
                                                <label class="screen-reader-text" for="widget-search">Search for:</label>
                                                <input id="widget-search" type="text" value="" name="search"
                                                    class="form-control" placeholder="type keyword here">
                                            </div>
                                            <button type="submit" class="theme_button color1">Search</button>
                                        </form>
                                    </div>

                                </div>
                                <!-- .col-* -->
                            </div>
                            <!-- .row -->


                            <div class="table-responsive">
                                <table class="table table-striped table-bordered">
                                    <tr>
                                        <td class="media-middle text-center">
                                            <input type="checkbox">
                                        </td>
                                        <th>Picture:</th>
                                        <th> Name:</th>
                                        <th>Candidate No:</th>
                                        <th>Age:</th>
                                        <th>Vote:</th>
                                        <th>Action:</th>
                                    </tr>
                                    @foreach ($candidates as $candidate)

                                    <tr class="item-editable">
                                        <td class="media-middle text-center">
                                            <input type="checkbox">
                                        </td>
                                        <td>
                                            <div class="">
                                                <img src="{{ $candidate->photoUrl }}" style="height: 50px; width: 50px; object-fit: cover; border-radius: 5px;" alt="">
                                            </div>
                                        </td>
                                        <td class="media-middle">
                                            <h5>{{ $candidate->firstName }} {{ $candidate->lastName }}</h5>
                                        </td>
                                        <td class="media-middle">
                                            <h5>{{ $candidate->number }}</h5>
                                        </td>
                                        <td class="media-middle">
                                            {{ $candidate->age }}
                                        </td>
                                        <td class="media-middle">
                                            {{ $votes[$candidate->id] }}
                                        </td>
                                        <td class="media-middle">
                                            <a href="{{ route('candidate.destroy', $candidate->number) }}"><i class="rt-icon2-trash-o"></i></a>
                                            <a href="{{ route('dashboard.edit', $candidate->id) }}" ><i class="rt-icon2-edit"></i></a>
                                        </td>
                                    </tr>

                                    @endforeach


                                </table>
                            </div>
                            <!-- .table-responsive -->
                        </div>
                        <!-- .with_border -->
                    </div>
                    <!-- .col-* -->
                </div>

            </div>

        </div>
        </div>
        <!-- .col-* -->

        </div>
        <!-- .row -->

        <!-- .container -->
    </section>


    </div>
    <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->


    <!-- chat -->
    <div class="side-dropdown side-chat dropdown">
        <a class="side-button side-chat-button" id="chat-dropdown" data-target="#" href="#"
            data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-comments-o"></i>
        </a>
    </div>
    <!-- .chat-dropdown -->
@endsection

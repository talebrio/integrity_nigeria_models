@extends('layouts.dashboard')

<style>
    input {
        text-transform: none !important;
    }
</style>

@section('content')
    <section class="ds with_bottom_border">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <ol class="breadcrumb darklinks">
                        <li>
                            <a href="#">Dashboard</a>
                        </li>
                        <li class="active">Candidates</li>
                    </ol>
                </div>
                <!-- .col-* -->
                <div class="col-md-6 text-md-right">
                    <span class="dashboard-daterangepicker">
                        <i class="fa fa-calendar"></i>
                        <span></span>
                        <i class="caret"></i>
                    </span>
                </div>
                <!-- .col-* -->
            </div>
            <!-- .row -->
        </div>
        <!-- .container -->
    </section>

    <section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
        <div class="container-fluid">

            <div class="row">
                <div class="col-sm-12">
                    <h3>Add Candidate</h3>
                </div>
            </div>
            <!-- .row -->


            <form class="form-horizontal" action="{{ route('dashboard.store') }}" enctype="multipart/form-data"
                method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-8">
                        <div class="with_border with_padding">
                            @if (session('success'))
                                <div class="alert alert-success">{{ session('success') }}</div>
                            @endif
                            <hr>


                            <div class="row form-group">
                                <label class="col-lg-3 control-label">First Name: </label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="fname" value="{{ old('fname') }}">
                                </div>
                                @error('fname')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="row form-group">
                                <label class="col-lg-3 control-label">Last Name: </label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="lname" value="{{ old('lname') }}">
                                </div>
                                @error('lname')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="row form-group">
                                <label class="col-lg-3 control-label">Picture: </label>
                                <div class="col-lg-9">
                                    <input type="file" accept="image/*" name="photoUrl" class="form-control"
                                        value="{{ old('photoUrl') }}">
                                </div>
                                @error('photoUrl')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="row form-group">
                                <label class="col-lg-3 control-label">candidate No: </label>
                                <div class="col-lg-9">
                                    <input type="number" class="form-control" name="number" value="{{ old('number') }}">
                                </div>
                                @error('number')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="row form-group">
                                <label class="col-lg-3 control-label">Age: </label>
                                <div class="col-lg-9">
                                    <input type="number" class="form-control" name="age" value="{{ old('age') }}">
                                </div>
                                @error('age')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="row form-group">
                                <label class="col-lg-3 control-label">Status: </label>
                                <div class="col-lg-9">
                                    <select name="status" id="">
                                        <option value="active">active</option>
                                        <option value="inactive">inactive</option>
                                    </select>
                                </div>
                                @error('status')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="row">
                                <div class="col-sm-12 text-right">
                                    <button type="submit" class="theme_button wide_button">Create candidate</button>
                                    <a href="{{ route('dashboard.show') }}"
                                        class="theme_button inverse wide_button">Cancel</a>
                                </div>
                            </div>

                            <!-- .row  -->

                        </div>
                        <!-- .with_border -->

                    </div>
                    <!-- .col-* -->


                    <!-- .col-* -->


                </div>
                <!-- .row  -->


            </form>

        </div>
        <!-- .container -->
    </section>
@endsection
